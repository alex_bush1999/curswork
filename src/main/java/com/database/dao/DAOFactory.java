package com.database.dao;

public abstract class DAOFactory {
    public static final int MYSQL = 1;


    public abstract AdvertDAO getAdvertDAO();

    public abstract RubricDAO getRubricDAO();

    public abstract UserDAO getUserDAO();

    public abstract AddressDAO getAddressDAO();


    public static DAOFactory getDAOFactory(int factoryId) {
        switch (factoryId) {
            case 1:
                return new MySQLDAOFactory();

            default:
                return null;
        }
    }
}
