package com.database.dao;


import java.util.List;

public interface AddressDAO<T> {
    T getAddressById(int id);
    void saveAddress(T address);
    void deleteAddress(T address);
    List<T> getAll();
}
