package com.database.dao;

import com.database.domain.Rubric;

public class MySQLDAOFactory extends DAOFactory {

    @Override
    public RubricDAO<Rubric> getRubricDAO() {
        return new MySQLRubricDAO();
    }

    @Override
    public UserDAO getUserDAO() {
        return null;
    }

    @Override
    public AddressDAO getAddressDAO() {
        return null;
    }

    @Override
    public AdvertDAO getAdvertDAO() {
        return new MySQLAdvertDAO();
    }
}
