package com.database.dao;


import java.util.List;

public interface AdvertDAO<T> {

    void saveAdvert(T advert);

    void deleteAdvert(T advert);

    void updateAdvert(T advert);

    List<T> getAllAdvert();
}
