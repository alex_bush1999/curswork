package com.database.dao;

public interface UserDAO<T> {
    T getUser();
    void saveUser(T user);
}
