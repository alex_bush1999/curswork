package com.database.dao;

public interface RubricDAO<T> {
    void saveRubric(T rubric);
    void deleteRubric(T rubric);

}
