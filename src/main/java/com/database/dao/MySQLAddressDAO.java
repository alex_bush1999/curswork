package com.database.dao;

import com.database.domain.Address;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public class MySQLAddressDAO implements AddressDAO<Address> {


/*
*
* public class BookUnitSession {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("BookStoreUnit");
    private static final ThreadLocal<EntityManager> tl = new ThreadLocal<EntityManager>();

    public static EntityManager getEntityManager() {
        EntityManager em = tl.get();

        if (em == null) {
            em = emf.createEntityManager();
            tl.set(em);
        }
        return em;
    }*/

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("call_board");

    EntityManager em = factory.createEntityManager();


    @Override
    public Address getAddressById(int id) {
        return null;
    }

    @Override
    public void saveAddress(Address address) {
        em.persist(address);
    }

    @Override
    public void deleteAddress(Address address) {


    }

    @Override
    public List<Address> getAll() {
        return null;
    }
}
