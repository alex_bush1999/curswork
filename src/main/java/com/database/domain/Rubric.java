package com.database.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Rubric implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @Column(name="rubric_name")
    private String name;//unique

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "rubric")
    private List<Advert> adverts = new ArrayList<>();

    public Rubric(String name) {
        this.name = name;
    }

    public Rubric() {
    }

    public List<Advert> getAdverts() {
        return adverts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAdverts(List<Advert> adverts) {
        this.adverts = adverts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
