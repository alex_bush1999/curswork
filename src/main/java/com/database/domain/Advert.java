package com.database.domain;



import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
public class Advert implements Serializable {


    @Id
    @GeneratedValue
    private int advert_id;


    @Column(name = "advert_name")
    String name;

    @Column(name = "author_text")
    String text;

    LocalDateTime dateTime;

    @ManyToOne()
    @JoinColumn(name = "author_fk_id")
    Author author;

    @ManyToOne()
    @JoinColumn(name = "rubric_fk_id")
    Rubric rubric;


    public Advert(String name, String text, Author author, LocalDateTime dateTime) {
        this.name = name;
        this.text = text;
        this.author = author;
        this.dateTime = dateTime;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Rubric getRubric() {
        return rubric;
    }

    public void setRubric(Rubric rubric) {
        this.rubric = rubric;
    }
}
