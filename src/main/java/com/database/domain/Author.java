package com.database.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Author implements Serializable {


    @Id
    @GeneratedValue
    private int author_id;

    @Column(name = "author_name")
    private String name;

    @Column(name = "author_phone")
    private String phone;

    @Column(name = "author_email")
    private String email;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "address_fk_id")
    private Address address;

    @OneToOne( mappedBy = "author")
    private User user;

    @OneToMany(cascade = CascadeType.PERSIST , mappedBy = "author")
    private List<Advert> advertList;

    public Author(String name, String phone, String email) {
        this.address = address;
        this.name = name;
        this.phone = phone;
        this.email = email;
        advertList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addAdvert(Advert advert)
    {
        advertList.add(advert);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
