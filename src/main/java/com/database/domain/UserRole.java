package com.database.domain;

public enum UserRole {
    ADMIN, SIMPLE_USER
}
