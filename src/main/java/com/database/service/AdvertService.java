package com.database.service;

import com.database.dao.AdvertDAO;
import com.database.dao.MySQLAdvertDAO;
import com.database.domain.Advert;

public class AdvertService {
    private MySQLAdvertDAO advertDAO;

    public AdvertService() {

    }

    public void saveAdvert(Advert advert) {
        advertDAO.saveAdvert(advert);
    }

    public void deleteAdvert(Advert advert)
    {
        advertDAO.deleteAdvert(advert);
    }

    public void updateAdvert(Advert advert)
    {
        advertDAO.updateAdvert(advert);
    }

    public  void getAllAdverts()
    {
        advertDAO.getAllAdvert();
    }

    //save advertDAO.save
}
