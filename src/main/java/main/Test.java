package main;

import com.database.domain.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.time.LocalDateTime;

public class Test {
    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("call_board");

        EntityManager em = factory.createEntityManager();

        EntityTransaction transaction = em.getTransaction();

        transaction.begin();

        Address address1 = new Address("Ukrain","Odessa","Levitana", 45, 123);

        Author author = new Author("Author1","390123129391", "petrov@gmail.com");

        User user = new User("sasha", "sasha1", UserRole.ADMIN);

        Advert advert = new Advert("myAdvert", "qweqweqwewqe", author, LocalDateTime.now());

        Rubric rubric =  new Rubric("TV");

        advert.setAuthor(author);
        advert.setRubric(rubric);
        user.setAuthor(author);
        author.setAddress(address1);

        em.persist(user);
        em.persist(address1);
        em.persist(author);
        em.persist(rubric);
        em.persist(advert);


   //     User user1 = em.find(User.class, 1);


        transaction.commit();
        em.close();
//        factory.close();
    }
}
